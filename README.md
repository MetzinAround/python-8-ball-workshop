# GitLab 8 ball app workshop

This repository contains the [GitLab 8 ball app workshop]() created by the GitLab for Education team.

This workshop can be conducted in person or completed asynchronously.

Coming soon: We'll open up the workshop for contributions as well.
