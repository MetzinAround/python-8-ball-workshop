---
title: Create a GitLab.com Account
nav_order: 1
---

# Sign up for a GitLab.com account
In order to complete the workshop, you'll need to have a GitLab.com account. If you already have an account on GitLab.com and wish to use your existing account, you do not need to complete this step.

# Validation
Please note, GitLab requires validation using a credit or debit card in order to use certain features, including CI/CD pipelines. We do not charge the card and we do not keep it on file to charge later. It is being used as a way to prevent <a href="https://about.gitlab.com/blog/2021/05/17/prevent-crypto-mining-abuse/" target="_blank">crypto mining abuse</a>. 

# Steps

1. Visit <a href="https://gitlab.com" target="_blank">https://gitlab.com</a>
2. Click on Register now or Sign in with an existing account.
3. Follow steps to create or log into your account.

[Next Page](https://devops-education.gitlab.io/workshops/python-8-ball-workshop/course/code-breakdown/)
